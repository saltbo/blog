---
title: "维基"
editLink: false
copyright: false
---

## 个人小册

- [如何学习 Go](https://booklets.saltbo.cn/#/how-to-go/)
- [如何学习 Python](https://booklets.saltbo.cn/#/how-to-python/)
- [如何学习 Rust](https://booklets.saltbo.cn/#/how-to-rust/)

## 收集的书

- [提问的智慧](https://booklets.saltbo.cn/#/how-to-ask-questions-the-smart-way/)
- [区块链小白书](https://blockchainlittlebook.com/)
